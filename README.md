# Accéder à la documentation en ligne

[Consulter le document](https://vjurie.gitpages.huma-num.fr/formation/index.html#/)

# Contexte
Support utilisé dans le cadre de la formation pour doctorant·e·s auprès du centre de formation de l'Université Paris Cité   
[<img src="figure/upc.png" width="80" style="background:none; border:none; box-shadow:none;">](https://u-paris.fr/) [<img src="figure/logo_geoteca.png" width="200" style="background:none; border:none; box-shadow:none;">](http://geoteca.u-paris.fr/)
